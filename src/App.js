import logo from './logo.svg';
import './App.css';
import Movie_theater from './Movie_theater/Movie_theater';

function App() {
  return (
    <div className="App">
        <Movie_theater/>
    </div>
  );
}

export default App;
