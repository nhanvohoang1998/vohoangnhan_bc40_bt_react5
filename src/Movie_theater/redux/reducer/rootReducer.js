import { combineReducers } from "redux";
import { seatsReducer } from "./seatsReducer";

export const rootReducer = combineReducers({
    seatsReducer,
})