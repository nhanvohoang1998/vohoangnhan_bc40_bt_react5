import { dataSeats } from "../../data_Seats"
let initialValue = {
    listSeats: dataSeats,
    cart:[]
}

export const seatsReducer = (state= initialValue, action)=>{
    switch(action.type){
        case 'CHOOSE_SEAT': {
            console.log(action)
            let cloneListSeats = [...state.listSeats];
            cloneListSeats.map((item)=>{
                let index =  item.danhSachGhe.findIndex((seat)=>{
                    return (seat.soGhe === action.payload.soGhe)
                })
                if(index != -1){
                    item.danhSachGhe[index].daDat = !item.danhSachGhe[index].daDat
                }
            })
            state.listSeats = cloneListSeats
            return {...state}
        }

        case 'ADD_TO_CART': {
            let combineNewCart = []
            state.listSeats.map((item)=>{
                let newCart = item.danhSachGhe.filter((seat)=>{
                    return seat.daDat === true
                })
                combineNewCart = [...combineNewCart, ...newCart]
            })
            return {...state, cart:combineNewCart}
        }

        default: return state
    }
}