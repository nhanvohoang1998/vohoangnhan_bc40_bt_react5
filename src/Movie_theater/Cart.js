import React, { Component } from 'react'
import { connect } from 'react-redux'

class Cart extends Component {
  renderCart = ()=>{
    return this.props.cart.map((item)=>{
      return (
        <tr>
          <td>{item.soGhe}</td>
          <td>{item.soGhe}</td>
          <td>{item.gia}</td>
        </tr>
      )
    })
  }
  render() {
    return (
      <div>
        <button onClick={this.props.handleAddToCart} className='btn btn-warning mb-3'>Confirm Selection</button>
        <table className='table text-white'>
          <thead>
            <th><h5>HÀNG</h5></th>
            <th><h5>SỐ GHẾ</h5></th>
            <th><h5>GIÁ</h5></th>
          </thead>
          <tbody>
            {this.renderCart()}
          </tbody>
        </table>
      </div>
    )
  }
}

let mapStateToProps = (state) =>{
  return {
    cart: state.seatsReducer.cart
  }
}

let mapDispatchToProps = (dispatch)=>{
  return{
    handleAddToCart: ()=>{
      let action ={
        type: 'ADD_TO_CART',
      }
      dispatch(action)
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Cart);
