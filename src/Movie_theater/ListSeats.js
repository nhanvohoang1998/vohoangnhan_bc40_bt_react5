import React, { Component } from 'react'
import { connect } from 'react-redux'
import Seat from './Seat'

class ListSeats extends Component {
    renderSeatCol = ()=>{
        let i = 0
        return this.props.Seats[0].danhSachGhe.map((seatCol)=>{
            i++
            return (
                <th className='text-warning'>{i}</th>
            )
            
        })
    }

    
    renderSeatRow = () => {
        return this.props.Seats.map((seatRow)=>{
            return (
                <tr>
                    <td className='text-warning'>{seatRow.hang}</td>
                    <Seat indexSeat={seatRow.danhSachGhe}/>
                </tr>
            )
        })
    }

    render() {
        return (
            <div className='.w3ls-reg'>
                <table className='table text-white'>
                    <tr>
                        <td></td>
                        {this.renderSeatCol()}
                    </tr>
                    {this.renderSeatRow()}
                </table>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        Seats: state.seatsReducer.listSeats
    }
}

export default connect(mapStateToProps, null)(ListSeats)