import React, { Component } from 'react'
import Cart from './Cart'
import ListSeats from './ListSeats'

export default class Movie_theater extends Component {
  render() {
    return (
      <div>
        <h1 className='text-white my-3'>MOVIE SEAT SELECTION</h1>
        <div className='container'>
          <h3 className='text-warning bg-white'>Screen this way</h3>
        </div>
        <div className='container'>
            <ListSeats/>
            <Cart/>
        </div>
      </div>
    )
  }
}
