import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

class Seat extends Component {

    rednerSeat = ()=>{
        return this.props.indexSeat.map((seat)=>{
            return (
                <td onChange={()=>{this.props.handlePushToCart(seat)}} style={{cursor: 'pointer'}}>
                  <input type="checkbox" value={seat.soGhe} />
                </td>
            )
        })
    }
  render() {
    return (
      <Fragment>
        {this.rednerSeat()}
      </Fragment>
    )
  }
}

let mapDispatchToProps = (dispatch)=>{
  return{
    handlePushToCart: (seat) =>{
      let action = {
        type: "CHOOSE_SEAT",
        payload: seat
      }
      dispatch(action)
    }
  }
}
export default connect(null, mapDispatchToProps)(Seat)